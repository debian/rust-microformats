#![cfg(test)]

use std::str::FromStr as _;

use super::*;
use tracing_test::traced_test;

#[traced_test]
#[test]
fn property_decl_kind_list_from_string() {
    assert_eq!(
        DeclKind::from_str("p-name"),
        vec![DeclKind::Plain("name".to_owned())]
    );

    assert_eq!(DeclKind::from_str("H-place"), vec![]);
    assert_eq!(DeclKind::from_str("value-title"), vec![]);

    assert_eq!(
        DeclKind::from_str("P-name zoom x-foo e-content"),
        vec![DeclKind::Hypertext("content".to_owned())]
    );
}

#[traced_test]
#[test]
fn property_decl_kind_from_prefix_and_name() {
    assert_eq!(
        DeclKind::Plain("default-to-plain".to_string()),
        DeclKind::from_prefix_and_name("x", "default-to-plain"),
        "defaults to plain text property"
    );
}

#[test]
fn adjust_timestamps_in_properties() -> Result<(), crate::Error> {
    let mut props = Properties::from_iter(vec![
        (
            "updated".to_string(),
            vec![PropertyValue::Temporal(temporal::Value::Timestamp(
                temporal::Stamp::now(),
            ))],
        ),
        (
            "published".to_string(),
            vec![PropertyValue::Temporal(temporal::Value::Timestamp(
                temporal::Time::from_str(" 10pmZ")?.into(),
            ))],
        ),
    ]);

    adjust_timestamps(&mut props);

    assert!(props
        .values()
        .flat_map(|values| values.iter())
        .all(|value| matches!(
            value,
            PropertyValue::Temporal(temporal::Value::Timestamp(ts)) if ts.is_stamp()
        )));

    Ok(())
}
