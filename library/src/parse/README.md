## Coverage
This library _does not_ pass the [main test suite][1]. The list of tests that are marked as
ignored in order to have a passing test suite [are listed on disk][2] and the reasoning lives
below:

> **Icons**: 🏳️ represents a false negative

- This parser has a more strict whitespace removal approach:
  * `microformats-v2/h-adr/geo`
  * `microformats-v2/h-feed/simple`
  * 🏳️ `microformats-v2/h-feed/implied-title`
  * `microformats-v2/h-recipe/all`
  * `microformats-v2/rel/duplicate-rels`
  * `microformats-v2/h-product/aggregate`
  * `microformats-v2/h-review/vcard`
  * `microformats-v2/h-review-aggregate/simpleproperties`
  * `microformats-v2/h-review-aggregate/justahyperlink`
- The [HTML serialization library][swc_html_codegen] is not optimized to the spec:
  * `microformats-v2/h-entry/encoding` - used converts HTML entities into its UTF-8 symbol
  * `microformats-v2/h-entry/urlincontent` - logic to rewrite URL not yet applied
- A need for consistent serialization of timezones
  * 🏳️ `microformats-v2/h-event/dates`

In addition to the tests provided, other tests based on other users' markup is tested here in the
`adhoc` module.

[1]: https://github.com/microformats/tests
[2]: ../test/ignored_generated_tests.txt
[swc_html_codegen]: https://crates.io/crates/swc_html_codegen
